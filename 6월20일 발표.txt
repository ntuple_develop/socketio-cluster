### socket.io-cluter PBL 진행 부분 발표 ###

 #.git 주소
  -git clone https://bitbucket.org/ntuple_develop/socketio-cluster.git

 #.설명은 README.md 파일에 정의해 놨음

 #.사전 설치

 #.pm2-config.json 설명
  -NODE_ENV = "production", "development"
  -서버 재시작시 pm2 최초 실행은 os의 서비스 사용해서 가동

 #.test url 설명
  -polling, webscoket 2가지 방법 모두 테스트를 위한 html 파일 분류
  -차이는 socket.io connection transport 옵션 뿐임

 #.npm run script 부분 : 실행파일 기능별로 분류하고 실행을 npm run으로 실행
  -1~7은 단계별로 점진적으로 기능을 add 하면서 발전시킴

 #.1.cluster 테스트 부분 : npm run test:cluster
  -https://nodejs.org/api/cluster.html
  -http://stackabuse.com/setting-up-a-node-js-cluster/

 #.2.socket.io : npm run test:socket.io
  -https://socket.io/get-started/chat/
  -http://localhost:8001/index.html
  -http://localhost:8001/polling.html
  -index.html, polling.html 오픈해서 확인
  -ServiceWorker.js
  -transport 옵션 부분 체크
  -크롬 개발자 도구 창에서 websocket frame 확인하는 방법

  #.3.socket.io-cluster : npm run test:socket.io-cluster
   -아래의 3가지 부분 않되는 경우가 어떻게 되는지 확인
    ㄱ.같은 프로세스(같은 클러스터)인 경우에만 socket 메시지를 전달 받을 수 있음
    ㄴ.transport 방식을 명시하지 않을 경우 polling으로 한번 체크해서 ws로 protocol 체인지 하는데 이때 동일한 프로세스(클러스트)에게 요청이 않갈 경우 에러 발생
    ㄷ.sticky session을 적용하기 전이라 long polling시 기존에 맺었던 프로세스에게 요청이 가지 않을 경우에 "Session ID unknown" 에러가 발생됨

  #.4.sticky-session1 : npm run test:sticky-session1
   -라이브러리 소스를 확인해서 대략 어떻게 되어있는지 확인하기

  #.5.sticky-session2 : npm run test:sticky-session2

  #.6.redis : npm run test:redis

  #.7.start : npm run start