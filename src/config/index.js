'use strict';

const config = {};

// API prefix URL
config.API_PREFIX_URL = '/api';

// 로그파일 포맷
config.LOGFILE_FORMAT = 'YYYY-MM-DD HH:mm:ss.SSS';

// 로그파일 이름
config.LOG_FILE_NAME = 'app.log';

// 로그파일 max size
config.LOG_MAX_FILE_SIZE = 10485760;

// 로그파일 rolling 기준 파일 갯수
config.LOG_MAX_FILE_COUNT = 3;

// redis host 
config.REDIS_HOST = 'localhost';

// redis port
config.REDIS_PORT = '6379';

module.exports = config;