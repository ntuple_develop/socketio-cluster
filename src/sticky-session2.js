'use strict';

const logger = require('./utils/logger');
const cluster = require('cluster');
let clusterForkSize = require('os').cpus().length;
const process = require('process');
const sticky = require('socketio-sticky-session');

// PORT 아규먼트가 전달이 않되어있을때는 8000 PORT를 default로 server run
let SERVER_PORT = process.env.PORT || 8000;

logger.info('======= cluster.js start : ' + (cluster.isMaster ? 'MASTER' : 'WORKER') + ' =======');
logger.info('cup clusterForkSize : ' + clusterForkSize);
logger.info('process info : [' + process.pid + '], cluster id : [' + (cluster.worker ? cluster.worker.id : 'master') + ']');

let app = null;
let server = null;
try {
    app = require('./app');
    server = require('http').Server(app);
} catch (error) {
    logger.error('app init error : ' + error + error.stack ? ' stack : ' + error.stack : '');
    process.exit(-1);
}

const stickyOption = {
    num: clusterForkSize
};

sticky(stickyOption, function () {
    const io = require('socket.io')(server);
    io.on('connection', function (socket) {
        logger.info('process id : [' + process.pid + '], cluster id : [' + cluster.worker.id + ']' + 'socket connected : ' + socket.id);
        socket.on('chat message', function (msg) {
            logger.info('[' + process.pid + '] [' + cluster.worker.id + '] [' + socket.id + ']: ' + msg, 'blue');
            io.emit('chat message', '[' + process.pid + '] [' + cluster.worker.id + '] [' + socket.id + ']: ' + msg);
        });
        socket.on('disconnect', function () {
            logger.info('socket disconnect : ' + socket.id);
        });
    });
    return server;
}).listen(SERVER_PORT, function () {
    logger.info('stickyserver start : ' + SERVER_PORT);
});
